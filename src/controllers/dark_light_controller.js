import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ["icon"]
    static values = {mode: String}
    connect() {
        console.log("Dark light controller is connected")
    }

    toggleMode() {
        this.modeValue = (this.modeValue == "light") ? "dark" : "light";
    }

    modeValueChanged() {
        let mapping = {"light":"fa fa-sun", "dark":"fa fa-moon"};
        this.iconTarget.className = mapping[this.modeValue];
    }

}
