import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ["snackbar"]
    static timerTimeOut;
    connect() {
        console.log("Snackbar controller is connected")
    }
    show() {
        this.snackbarTarget.classList.remove('show');
        clearTimeout(this.timerTimeOut);
        setTimeout(() => {
            this.snackbarTarget.classList.add('show');
            this.timerTimeOut = setTimeout(() => {
                this.snackbarTarget.classList.remove('show');
            }, 2900)
        }, 10);

    }

}
