import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ["container", "slide", "dot"]
    static values = {index: Number}
    static timerInterval

    connect() {
        console.log("Slideshow controller is connected")
    }

    prev() {
        this.indexValue = (this.indexValue == 1) ? this.slideTargets.length : this.indexValue - 1
    }

    next() {
        this.indexValue = (this.indexValue == this.slideTargets.length) ? 1 : this.indexValue + 1
    }

    setIndex(e) {
        this.indexValue = e.currentTarget.dataset.id
    }

    indexValueChanged() {
        clearInterval(this.timerInterval)
        this.timerInterval = setInterval(() => {
            this.next();
        }, 1000)
        const slide = this.slideTargets[this.indexValue-1]
        this.containerTarget.scrollTo(slide.offsetLeft, 0)
        this.dotTargets.forEach(dot => dot.dataset.id == this.indexValue ? dot.classList.add('active') : dot.classList.remove('active'))
    }
}
