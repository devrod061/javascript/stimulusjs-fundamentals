import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ["display"]
    static values = {contents: Array, index: Number, contentIndex: Number}
    static timerInterval
    connect() {
        console.log("Typewriter controller is connected")
        this.type();
    }

    type() {
        this.timerInterval = setInterval(() => {
            const string = this.contentsValue[this.indexValue]
            if (this.contentIndexValue < string.length) {
                this.displayTarget.innerHTML += string[this.contentIndexValue]
                this.contentIndexValue++
            } else {
                clearInterval(this.timerInterval);
                setTimeout(() => {
                    this.displayTarget.innerHTML = ''
                    this.indexValue = this.indexValue == this.contentsValue.length - 1 ? 0 : this.indexValue + 1
                    this.contentIndexValue = 0
                    this.type()
                }, 3000)
            }
        }, 50)
    }

}
