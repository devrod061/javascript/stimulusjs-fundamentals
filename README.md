# Stimulus Miniatures

> My mini-projects with StimulusJS

StimulusJS is ...

## Project Snapshots

![image](https://user-images.githubusercontent.com/55923773/118135825-0892bc80-b436-11eb-8838-d13937b356bc.png)

## Presentation
- Live Link: https://sheltered-castle-36920.herokuapp.com/
- Tutorial Link:

## Feature Outline

#### 1. Section 1

|No|Name|Learning points|
|---|---|---|
|1|Copy to Clipboard||
|2|Accordian||
|3|Counter App||
|4|Dark/Light mode||
|5|Price Slider||
|6|Select / Deselect All||
|7| Progress Bar||
|8| Countdown Timer|
|9|Snackbar||

##### 2. Section 2

|No|Name|Learning points|
|---|---|---|
|10|Todo List||
|11|TypeWriter Effect||
|12|Modal||
|13|Pomodoro App||
|14|Scroll Indicator||
|15|Slideshow||

## Built With

- StimulusJS

## Deployment

1. Git clone this repo and cd the to the directory.
2. Run `yarn install` in command line to install gems.
3. Run `yarn start` and open browser with `http://localhost:9000/`.
4. Enjoy :)



## Authors

👤 **Kyle Law**

- Github: [@Kyle-Law](https://github.com/Kyle-Law)
- Linkedin: [Kyle law](https://www.linkedin.com/in/kyle-lawzhunkhing/)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Microverse
- [Nelson Sakwa on Behance](https://www.behance.net/sakwadesignstudio)

## 📝 License

This project is [MIT](LICENSE) licensed.
